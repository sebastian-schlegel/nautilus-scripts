#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
# Copyright (c) 2013 Sebastian Schlegel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of this software nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

"""
This is a script to be used with nautilus.

Usage:
1. Select files and/or directories in nautilus.
2. Start the script.

This script will search for 'compiled' python files in your selections and delete them. Directories will be searched recursively.
To not delete important files by accident, this script will not delete files if no .py file with same filename is found in the directory.
'__pycache__' directories are an exception from this rule.
To disable this ruleset, just make the function 'can_savely_remove' always return True.
"""

import os
import gtk


def on_info(message):
    md = gtk.MessageDialog(None, 
        gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO, 
        gtk.BUTTONS_CLOSE, str(message))
    md.run()
    md.destroy()
    
def on_error(message):
    md = gtk.MessageDialog(None, 
        gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR, 
        gtk.BUTTONS_CLOSE, str(message))
    md.run()
    md.destroy()
    
def is_pyc (filename):
    return f.endswith(".pyc") or f.endswith(".pyo")
    
def can_savely_remove (filename, path, other_files_in_directory):
    return f[0:-4]+".py" in other_files_in_directory or "__pycache__" in path
    
def delete_file (filename, path):
    f = path + "/" + filename
    if os.access(f, os.W_OK):
        os.remove(f)
        return True
    else:
        return False

try:
    selected_files = os.environ.get("NAUTILUS_SCRIPT_SELECTED_FILE_PATHS").split("\n")

    removed = 0
    unsave = 0
    no_access = 0
    
    for selected_file in selected_files:
        if os.path.isdir(selected_file):
            for path, dirs, files in os.walk(selected_file):
                for f in files:
                    if is_pyc(f):
                        if can_savely_remove(f, path, files):
                            if delete_file(f, path):
                                removed += 1
                            else:
                                no_access += 1
                        else:
                            unsave += 1
        elif os.path.isfile(selected_file):
            f = os.path.split(selected_file)[1]
            path = os.path.split(selected_file)[0]
            files = os.listdir(os.path.split(selected_file)[0])
            if is_pyc(f):
                if can_savely_remove(f, path, files):
                    if delete_file(f, path):
                        removed += 1
                    else:
                        no_access += 1
                else:
                    unsave += 1
    
    if removed > 0:
        message = "%d kompilierte Python Dateien entfernt.\n"%removed
    elif removed == 0 and no_access == 0 and unsave == 0:
        message = "Keine kompilierten Python Dateien gefunden.\n"
        message += "Bitte beachten Sie, dass nur markierte Dateien und Ordner berücksichtigt werden.\n"
    else:
        message = "Keine Dateien entfernt.\n"
    if no_access > 0:
        message += "\n%d Dateien konnten nicht entfernt werden: Zugriff verweigert.\n"%no_access
    if unsave > 0:
        message += "\n%d Dateien wurden nicht entfernt, da keine zugehörige Quellcode Dateien gefunden wurden.\n"%unsave
    
    on_info(message)
    
except Exception as e:
    on_error(e)
    
