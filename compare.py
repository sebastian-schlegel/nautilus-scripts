#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
# Copyright (c) 2013 Sebastian Schlegel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of this software nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

"""
This is a script to be used with nautilus.

Usage:
1. Select two or more files or two directories in nautilus.
2. Start the script.

Dependent on the selected files or directories, meld or diffuse will be started to compare your selections.
"""

import os
import sys
import gtk
    
def on_error(message):
    md = gtk.MessageDialog(None, 
        gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR, 
        gtk.BUTTONS_CLOSE, str(message))
    md.run()
    md.destroy()
    
def selection_type(selections):
    if os.path.isfile(selections[0]):
        query = os.path.isfile
        type_str = "file"
    elif os.path.isdir(selections[0]):
        query = os.path.isdir
        type_str = "dir"
    for selection in selections:
        if not query(selection):
            return ""
    return type_str

try:
    has_meld = bool(os.popen("which meld").readlines())
    has_diffuse = bool(os.popen("which diffuse").readlines())
    
    files = os.environ.get("NAUTILUS_SCRIPT_SELECTED_FILE_PATHS").split("\n")
    del files[-1]
    
    type_str = selection_type(files)
    
    if type_str == "dir":
        if len(files) != 2:
            on_error("Cannot compare more than two directories at once.")
            sys.exit()
        elif has_meld:
            command = "meld '%s' '%s'"%(files[0], files[1])
        else:
            on_error("Please install meld for comparing directories.")
            sys.exit()
    elif type_str == "file":
        if len(files) == 2:
            # in order to make diffuse your default application for comparing two files,
            # just swap the next two lines with the two lines following them - while retaining if and elif ;-)
            if has_meld:
                command = "meld '%s' '%s'"%(files[0], files[1])
            elif has_diffuse:
                command = "diffuse '%s' '%s'"%(files[0], files[1])
            else:
                on_error("Please install meld or diffuse for comparing files.")
                sys.exit()
        elif has_diffuse:
            command = "diffuse"
            for f in files:
                command += " '" + f + "' "
        else:
            on_error("Please install diffuse to compare more than two files at once.")
            sys.exit()
    
    os.system(command)
    sys.exit()

except Exception as e:
    on_error(e)
    raise
