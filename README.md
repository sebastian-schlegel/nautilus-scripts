About
-----

Here are some of my nautilus scripts I use for my daily work with the nautlius file manager.
All the scripts are written in python.

2013 Sebastian Schlegel, MIT License

Website: [www.sebastian-schlegel.de](http://www.sebastian-schlegel.de)

Email: <hallo@sebastian-schlegel.de>

GNUPG-Key: [D3684AF4](http://keyserver.ubuntu.com:11371/pks/lookup?op=get&search=0x62578B9AD3684AF4)

Find me on: [facebook](https://www.facebook.com/johann.sebastian.schlegel) [google+](https://plus.google.com/100650131067590951056) [twitter](https://twitter.com/JSSchlegel) [xing](https://www.xing.com/profile/Sebastian_Schlegel14)

### Dependencies:

*   python (tested with python 2.7)

*   nautilus (tested with nautlius 3.6)
 
### Installation:

1. Download the files.

2. Copy them into the directory whre nautilus searches for extension scripts. This directory changes from version to version:

Nautilus 3.6: ~/.local/share/nautilus/scripts

Nautilus 3.X: ~/.local/share/nautilus-python/extensions/

Nautilus 2.X: ~/.gnome2/nautilus-scripts

_(I'm not really sure which version of nautilus uses which path.)_

3. Make them executable.

### Screenshot:

![Screenshot](https://www.sebastian-schlegel.de/images/artikelbilder/nautilusscripts.jpg)


* * *


compare.py
----------

Additional requirements:

*   python-gtk

*   meld and/or diffuse

Usage:

1. Select two or more files or two directories in nautilus.

2. Start the script.

Dependent on the selected files or directories, meld or diffuse will be started to compare your selections.

* * *

open_new_terminal_from_here.py
------------------------------

Additional requirements:

_None_

Usage:

1. Select a file or directory in nautilus.

2. Start the script.

If a file was selected: A new terminal will be started with the file's location as working directory.
If a directory was selected: A new terminal will be started with the selected directory as working directory.

* * *

remove_pyc_and_pyo_files.py
---------------------------

Additional requirements:

*   python-gtk

Usage:

1. Select files and/or directories in nautilus.

2. Start the script.

This script will search for 'compiled' python files in your selections and delete them. Directories will be searched recursively.
To not delete important files by accident, this script will not delete files if no .py file with same filename is found in the directory.
Dedicated pycache directories are an exception from this rule.
To disable this ruleset, just make the function 'can_savely_remove' always return True.





